package ru.ineedyourhelp.ineedyourhelpapp

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URI
import java.net.URL
import java.net.URLEncoder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myButt: Button = findViewById(R.id.buttonRegistration)
        myButt.setOnClickListener {

            var fieldLogin = fieldLogin.text.toString()
            var fieldEmail = fieldEmail.text.toString()
            var fieldPassword = fieldPassword.text.toString()
            var fieldPasswordDouble = fieldPasswordDouble.text.toString()

            Thread(Runnable {
                try {
                    sendPostRequest(fieldLogin, fieldEmail, fieldPassword, fieldPasswordDouble)
                } catch (ex: Exception) {
                    Log.d("Exception", ex.toString())
                }
            }).start()
        }

        val buttonProfile: Button = findViewById(R.id.buttonProfile)
        buttonProfile.setOnClickListener {
            val profileIntent = Intent(this, ProfileActivity::class.java)
            startActivity(profileIntent)
        }

        val buttonOrders: Button = findViewById(R.id.buttonOrders)
        buttonOrders.setOnClickListener {
            val ordersIntent = Intent(this, OrdersActivity::class.java)
            startActivity(ordersIntent)
        }

    }

//    fun createRegistration(view: View){
//        var editTextHell = fieldLogin.text.toString()
//        println("Login in fun is - $editTextHell")
//    }

    private fun sendPostRequest(fieldLogin:String, fieldEmail:String, fieldPassword:String, fieldPasswordDouble:String) {

        println("Login is - $fieldLogin $fieldEmail $fieldPassword $fieldPasswordDouble")

        var reqParam = URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(fieldLogin, "UTF-8")
        reqParam += "&" + URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(fieldEmail, "UTF-8")
        reqParam += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(fieldPassword, "UTF-8")
        reqParam += "&" + URLEncoder.encode("password_confirmation", "UTF-8") + "=" + URLEncoder.encode(fieldPasswordDouble, "UTF-8")

        val mURL = URL("https://seohazard.com/public/api/register?name=wwwwwwaa&email=wwwwww@wwwwww.ru&password=wwwwwwww&password_confirmation=wwwwwwww")

        with(mURL.openConnection() as HttpURLConnection) {
            // optional default is GET
            requestMethod = "POST"

            val wr = OutputStreamWriter(getOutputStream());
            wr.write(reqParam);
            wr.flush();

            println("URL : $url")
            println("Response Code : $responseCode")

            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()

                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                println("Response : $response")
            }
        }
    }

}